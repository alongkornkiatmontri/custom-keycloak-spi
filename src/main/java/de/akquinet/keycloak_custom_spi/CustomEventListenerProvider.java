package de.akquinet.keycloak_custom_spi;

import org.keycloak.email.DefaultEmailSenderProvider;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventType;
import org.keycloak.events.admin.AdminEvent;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.RealmProvider;
import org.keycloak.models.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Map;

public class CustomEventListenerProvider implements EventListenerProvider {

    private static final Logger logger = LoggerFactory.getLogger(CustomEventListenerProvider.class);

    private final KeycloakSession session;
    private final RealmProvider model;

    public CustomEventListenerProvider(KeycloakSession session) {
        this.session = session;
        this.model = session.realms();
    }

    @Override
    public void onEvent(Event event) {
        if (event.getType() == EventType.UPDATE_PASSWORD) {
            RealmModel realm = this.model.getRealm(event.getRealmId());
            UserModel user = this.session.users().getUserById(event.getUserId(), realm);
            if (user != null && user.getEmail() != null && user.isEmailVerified() && user.getUsername().startsWith("2")) {
                DefaultEmailSenderProvider senderProvider = new DefaultEmailSenderProvider(session);
                try {
                    logger.info(
                            "send installation guide email to customer with email address {}",
                            user.getEmail()
                    );
                    StringBuilder redirect_uri = new StringBuilder();

                    if (event.getDetails() != null) {
                        for (Map.Entry<String, String> e : event.getDetails().entrySet()) {
                            if (e.getKey().equals("redirect_uri")) {
                                if (e.getValue() == null || e.getValue().indexOf(' ') == -1) {
                                    redirect_uri.append(e.getValue());
                                    redirect_uri.append("/installation");
                                }
                            }
                        }
                    }

                    // Das ist nur eine vorläufige Lösung, um das Email Template zu erstellen,
                    // da es aktuell beim Einbinden von Velocity Template einen Fehler gibt
                    String htmlTemplate = "<!DOCTYPE html>" +
                            "<html lang='de'>" +
                            "<head>" +
                            "<meta charset='UTF-8'>" +
                            "<meta name='format-detection' content='telephone=no'>" +
                            "<link href='https://fonts.googleapis.com/css2?family=Scada:ital,wght@0,400;0,700;1,400;1,700&display=swap' rel='stylesheet'>" +
                            "<style>" +
                            ".container {" +
                            "   background-color: #f2f2f2;" +
                            "   max-width: 700px;" +
                            "   margin: 0 auto;" +
                            "   padding: 20px 30px;" +
                            "   font-family: Scada, Helvetica, Arial, sans-serif;" +
                            "   color: #385c6b;" +
                            "}" +
                            "h2 {" +
                            "   font-size: 16px;" +
                            "   color: #385c6b;" +
                            "}" +
                            ".mail-hint {" +
                            "   font-size: 14px;" +
                            "   color: #385c6b;" +
                            "   text-align: left;" +
                            "   font-style: italic;" +
                            "}" +
                            "h3 {" +
                            "   font-size: 14px;" +
                            "   color: #385c6b;" +
                            "   text-align: left;" +
                            "   margin: 10px 0 0;" +
                            "}" +
                            ".header {" +
                            "   padding-bottom: 15px;" +
                            "   text-align: center;" +
                            "}" +
                            ".footer {" +
                            "   max-width: 700px;" +
                            "   margin: 0 auto;" +
                            "   background-color: #385c6b;" +
                            "   color: white;" +
                            "   padding: 20px 30px;" +
                            "}" +
                            ".information-footer {" +
                            "   max-width: 700px;" +
                            "   margin: 0 auto;" +
                            "   color: #385c6b !important;" +
                            "}" +
                            ".header-text {" +
                            "   color: #385c6b" +
                            "}" +
                            ".button-form {" +
                            "   text-align: center;" +
                            "   padding: 20px 0;" +
                            "}" +
                            ".send-button {" +
                            "   background-color: #c30059;" +
                            "   color: white !important;" +
                            "   border-radius: 5px;" +
                            "   padding: 10px;" +
                            "   border: none;" +
                            "   cursor: pointer;" +
                            "   text-decoration: none;" +
                            "}" +
                            "</style>" +
                            "</head>" +
                            "<body>" +
                                "<div class='header'>" +
                                "   <h1 class='header-text'>kv.dox</h1>" +
                                "</div>" +
                                "<div class='container'>" +
                                    "<div>" +
                                        "<h1>Bitte installieren Sie jetzt kv.dox</h1>" +
                                        "<p>Sie haben kv.dox erfolgreich bestellt. Um den KIM-Dienst nutzen zu können, " +
                                            "folgen Sie bitte unserer Installationsanleitung." +
                                        "</p>" +
                                        "<div class='button-form'>" +
                                            "<a class='send-button' href=\"" + redirect_uri + "\">kv.dox installieren</a>" +
                                        "</div>" +
                                    "</div>" +
                                "</div>" +
                                "<div class='footer'>" +
                                    "<p>&copy; 2021 Kassenärztliche Bundesvereinigung (KBV)</p>" +
                                "</div>" +
                                "<div class='information-footer'>" +
                                    "<div style='float: left'>" +
                                        "<a style='color: #385c6b' href='https://kvdox.akquinet.de/?show=contact'>Benötigen Sie Hilfe?</a>" +
                                    "</div>" +
                                    "<div style='float: right'>" +
                                        "<a style='color: #385c6b' href='https://kvdox.akquinet.de/privacy'>Datenschutzrichtlinien</a> | <a style='color: #385c6b' href='https://kvdox.akquinet.de/imprint'>Impressum</a>" +
                                    "</div>" +
                                "</div>" +
                            "</body>" +
                            "</html>";
                    senderProvider.send(session.getContext().getRealm().getSmtpConfig(), user, "So installieren Sie kv.dox", null, htmlTemplate);
                } catch (Exception e) {
                    logger.error(
                            "Failed to send installation guide email to customer with email address {} :",
                            user.getEmail(),
                            e
                    );
                }
            }
        }
    }

    @Override
    public void onEvent(AdminEvent adminEvent, boolean b) {
    }

    @Override
    public void close() {
    }

    private String toString(Event event) {
        StringBuilder sb = new StringBuilder();
        sb.append("type=");
        sb.append(event.getType());
        sb.append(", realmId=");
        sb.append(event.getRealmId());
        sb.append(", clientId=");
        sb.append(event.getClientId());
        sb.append(", userId=");
        sb.append(event.getUserId());

        if (event.getError() != null) {
            sb.append(", error=");
            sb.append(event.getError());
        }

        if (event.getDetails() != null) {
            for (Map.Entry<String, String> e : event.getDetails().entrySet()) {
                sb.append(", ");
                sb.append(e.getKey());
                if (e.getValue() == null || e.getValue().indexOf(' ') == -1) {
                    sb.append("=");
                    sb.append(e.getValue());
                } else {
                    sb.append("='");
                    sb.append(e.getValue());
                    sb.append("'");
                }
            }
        }
        return sb.toString();
    }
}
